package com.mindex.challenge.service.impl;

import java.util.ArrayDeque;
import java.util.List;
import java.util.Queue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mindex.challenge.dao.EmployeeRepository;
import com.mindex.challenge.data.Employee;
import com.mindex.challenge.data.ReportingStructure;
import com.mindex.challenge.service.ReportingStructureService;


@Service
public class ReportingStructureServiceImpl implements ReportingStructureService {

	private static final Logger LOG = LoggerFactory.getLogger(EmployeeServiceImpl.class);
	
	@Autowired
    private EmployeeRepository employeeRepository;

	@Override
	public ReportingStructure read(String id) {
		LOG.debug("Reporting Structure start for id [{}]", id);
		
		ReportingStructure reportingStructure = new ReportingStructure();
		reportingStructure.setEmployeeId(id);
		int numberOfReports = 0;

		//Get employee
		Employee currentEmployee = findEmployee(id);
		
		//Get number of reports for employee using breadth first search
		numberOfReports = bFSforNumberofReports(currentEmployee);
		
		reportingStructure.setNumberOfReports(numberOfReports);
		return reportingStructure;
	}
	
	private int bFSforNumberofReports(Employee currentEmployee) {
		int numberOfReports = 0;
		Queue<Employee> employeeQueue = new ArrayDeque<>();
		employeeQueue.add(currentEmployee);
		
		//BFS through all employees to get numberOfReports
		while(!employeeQueue.isEmpty()) {
			currentEmployee = employeeQueue.poll();
			List<Employee> directReports = currentEmployee.getDirectReports();
			if(directReports != null) {
				for(Employee e : directReports) {
					currentEmployee = findEmployee(e.getEmployeeId());
					numberOfReports++;
					employeeQueue.add(currentEmployee);
				}
			}
		}
		return numberOfReports;
	}
	
	private Employee findEmployee(String id) {
		LOG.debug("Reading employee with id [{}]", id);
		Employee employee = employeeRepository.findByEmployeeId(id);
		
		if (employee == null) {
            throw new RuntimeException("Invalid employeeId in Reporting Structure: " + id);
        }
		
		return employee;
	}

}
