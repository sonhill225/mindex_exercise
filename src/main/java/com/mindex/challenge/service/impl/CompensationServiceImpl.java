package com.mindex.challenge.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mindex.challenge.dao.CompensationRepository;
import com.mindex.challenge.dao.EmployeeRepository;
import com.mindex.challenge.data.Compensation;
import com.mindex.challenge.data.Employee;
import com.mindex.challenge.service.CompensationService;

@Service
public class CompensationServiceImpl implements CompensationService {

	private static final Logger LOG = LoggerFactory.getLogger(EmployeeServiceImpl.class);

    @Autowired
    private CompensationRepository compensationRepository;
    
    @Autowired
    private EmployeeRepository employeeRepository;
    
    @Override
    public Compensation create(Compensation compensation) {
        LOG.debug("Creating compensation [{}]", compensation);
        
        Employee employee = employeeRepository.findByEmployeeId(compensation.getEmployeeId());

        if (employee == null) {
            throw new RuntimeException("Employee does not exist - cannot create compensation: " + compensation.getEmployeeId());
        }
        
        Compensation compensationDupCheck = compensationRepository.findByEmployeeId(compensation.getEmployeeId());

        if (compensationDupCheck != null) {
            throw new RuntimeException("Cannot create 2 compensations for same employee: " + compensation.getEmployeeId());
        }

        compensationRepository.insert(compensation);
        
        return compensation;
    }

    @Override
    public Compensation read(String id) {
        LOG.debug("Reading compensation with id [{}]", id);

        Compensation compensation = compensationRepository.findByEmployeeId(id);

        if (compensation == null) {
            throw new RuntimeException("Invalid employeeId: " + id);
        }

        return compensation;
    }
    
}
