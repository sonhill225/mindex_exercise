package com.mindex.challenge.service.impl;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;

import com.mindex.challenge.data.ReportingStructure;
import com.mindex.challenge.service.ReportingStructureService;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ReportingStructureServiceImplTest {

    private String reportingStructureIdUrl;

    @Autowired
    private ReportingStructureService reportingStructureService;

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Before
    public void setup() {
    	reportingStructureIdUrl = "http://localhost:" + port + "/reporting-structure/{id}";
    	reportingStructureService = new ReportingStructureServiceImpl();
    }

    // 1) Testing Read
    @Test
    public void testRead() {
    	ReportingStructure reportingStructureJohn = restTemplate.
    			getForEntity(reportingStructureIdUrl, ReportingStructure.class, 
    					"16a596ae-edd3-4847-99fe-c4518e82c86f").getBody();		
    	ReportingStructure reportingStructurePaul = restTemplate.
    			getForEntity(reportingStructureIdUrl, ReportingStructure.class, 
    					"b7839309-3348-463b-a7e3-5de1c168beb3").getBody();
    	ReportingStructure reportingStructureRingo = restTemplate.
    			getForEntity(reportingStructureIdUrl, ReportingStructure.class, 
    					"03aa1462-ffa9-4978-901b-7c001562cf6f").getBody();
    	ReportingStructure reportingStructurePete = restTemplate.
    			getForEntity(reportingStructureIdUrl, ReportingStructure.class, 
    					"62c1084e-6e34-4630-93fd-9153afb65309").getBody();
    	ReportingStructure reportingStructureGeorge = restTemplate.
    			getForEntity(reportingStructureIdUrl, ReportingStructure.class, 
    					"c0c2293d-16bd-4603-8e08-638a9d18b22c").getBody();
    	
    	assertEquals(4,reportingStructureJohn.getNumberOfReports());
    	assertEquals(0,reportingStructurePaul.getNumberOfReports());
    	assertEquals(2,reportingStructureRingo.getNumberOfReports());
    	assertEquals(0,reportingStructurePete.getNumberOfReports());
    	assertEquals(0,reportingStructureGeorge.getNumberOfReports());
    }
    
    /* 2) Testing Read to make sure when employee cannot be found a NullPointerException 
     * is thrown (subclass of RuntimeException).
     */
    @Test (expected = NullPointerException.class) 
    public void readShouldThrowRuntimeExceptionWhenEmployeeIDIsInvalid() { 	
    		reportingStructureService.read("16a546ae-edd3-4247-99fe-c4338e82c86f"); 
    }

}
