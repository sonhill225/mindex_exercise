package com.mindex.challenge.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.math.BigDecimal;
import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;

import com.mindex.challenge.data.Compensation;
import com.mindex.challenge.service.CompensationService;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CompensationServiceImplTest {

	private String compensationUrl;
    private String compensationIdUrl;
    private CompensationService compensationService;

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Before
    public void setup() {
    	compensationUrl = "http://localhost:" + port + "/compensation";
    	compensationIdUrl = "http://localhost:" + port + "/compensation/{id}";
    	compensationService = new CompensationServiceImpl();
    }

    // 1) Testing Create and Read
    @Test
    public void testCreateReadUpdate() {
    	Compensation testCompensation = new Compensation();
    	testCompensation.setEmployeeId("16a596ae-edd3-4847-99fe-c4518e82c86f"); //John's employee id
    	testCompensation.setSalary(new BigDecimal("10000000.00"));
    	testCompensation.setEffectiveDate(LocalDate.now());

        // Create checks
    	// 1 returns employee
    	Compensation createdCompensation = restTemplate.
    			postForEntity(compensationUrl, testCompensation, Compensation.class).getBody();

        assertNotNull(createdCompensation.getEmployeeId());
        assertCompensationEquivalence(testCompensation, createdCompensation);

        // Read checks
        // 1 returns employee
        Compensation readCompensation = restTemplate.
        		getForEntity(compensationIdUrl, Compensation.class, createdCompensation.getEmployeeId()).getBody();
        assertEquals(createdCompensation.getEmployeeId(), readCompensation.getEmployeeId());
        assertCompensationEquivalence(createdCompensation, readCompensation);
        
        // 2 throws exception since no employee id exists for the compensation
    }
    
    /* 2) Testing Create to make sure when compensation cannot be found a NullPointerException 
     * is thrown (subclass of RuntimeException).
     */
    
    @Test (expected = NullPointerException.class) 
    public void createShouldThrowRuntimeExceptionWhenEmployeeIDIsInvalid() {
    	Compensation testCompensation = new Compensation();
    	testCompensation.setEmployeeId("16a546ae-edd3-4247-99fe-c4338e82c86f"); //nonexistent employee id
    	testCompensation.setSalary(new BigDecimal("1000000.00"));
    	testCompensation.setEffectiveDate(LocalDate.now());
    	
    	compensationService.create(testCompensation);
    }
    
    /* 3) Testing Read to make sure when compensation cannot be found a NullPointerException 
     * is thrown (subclass of RuntimeException).
     */
    @Test (expected = NullPointerException.class) 
    public void readShouldThrowRuntimeExceptionWhenEmployeeIDIsInvalid() { 	
    		compensationService.read("16a546ae-edd3-4247-99fe-c4338e82c86f"); 
    }

    private static void assertCompensationEquivalence(Compensation expected, Compensation actual) {
        assertEquals(expected.getEmployeeId(), actual.getEmployeeId());
        assertEquals(expected.getSalary(), actual.getSalary());
        assertEquals(expected.getEffectiveDate(), actual.getEffectiveDate());
    }
}
